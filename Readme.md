The folder json-3.7.3 contains the json library version 3.7.3, commit ad383f66bc48bac18ddf8785d63ef2c6a32aa770 from https://github.com/nlohmann/json.
The only additions are the other files in this folder. Furthermore, the folders test and benchmarks were deleted to save space.
For licensing information, see the corresponding files in the folder json-3.7.3.

This version has been tested for the following configurations:
	- Windows 10 using Microsoft Visual Studio 2017
	- Red Hat Linux 8.2.1 using gcc 8.2.1